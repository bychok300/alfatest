﻿//package android.scenarios;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AlfaTest {

    public static void getAscendingValuesFromFile(String pathToFile){
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathToFile));
            List<Integer> numbers = new ArrayList<>();
            String [] numAr = br.readLine().split(",");
            for(String strNumber : numAr){
                numbers.add(Integer.parseInt(strNumber));
            }
        br.close();
        Collections.sort(numbers); //можно так
        // или так
        //for (int i=0; i < numbers.size(); i++) {
        //    System.out.println(numbers.get(i));
        //}
        System.out.println(numbers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void getDescendingValuesFromFile(String pathToFile){
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathToFile));
            List<Integer> numbers = new ArrayList<>();
            String [] numAr = br.readLine().split(",");
            for(String strNumber : numAr){
                numbers.add(Integer.parseInt(strNumber));
            }
            br.close();
            Collections.sort(numbers); //можно так
            Collections.reverse(numbers);
            // или так
            //for (int i=0; i < numbers.size(); i++) {
            //    System.out.println(numbers.get(i));
            //}
            System.out.println(numbers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
    public static void factorial(int number) {
        long result = 1;

        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        System.out.println(result);
    }

    public static void main(String[] args) {
        getAscendingValuesFromFile("C:\\Users\\anton.bychek\\testui\\src\\test\\java\\android\\scenarios\\numbs.txt");
        getDescendingValuesFromFile("C:\\Users\\anton.bychek\\testui\\src\\test\\java\\android\\scenarios\\numbs.txt");
	factorial(20);
    }
}
